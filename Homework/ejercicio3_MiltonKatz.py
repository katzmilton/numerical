## EJERCICIO 3
## MILTON KATZ
## LU 368/16
##
import numpy as np
import matplotlib.pyplot as plt


#Defino func para el inciso a
def resuelve_y(y0,t0,T,N):

    h = (T-t0)/(N-1)
    t = np.arange(t0,T+h,h)

    y = [y0]

    for i in range(len(t)):
        y.append(y[i]+h*(-2*y[i]+t[i]**2))

    return [np.array(t),np.array(y[:-1])]

#Defino funcion para el inciso b
def res_opcB(y0,x0,t0,T,N):
    h = (T-t0)/(N-1)
    t = np.arange(t0,T+h,h)

    yB = [y0]
    xB = [x0]

    for i in range(len(t)):
        xB.append(xB[i]-2*h*yB[i])
        yB.append(yB[i]+h*xB[i+1])

    return [np.array(t),np.array(yB[:len(t)])]

#Defino funcion para el inciso c
def euler(f,y0,t0,T,N):
    h = (T-t0)/(N-1)
    t = np.arange(t0,T+h,h)

    y = [y0]

    for i in range(len(t)):
        y.append(y[i]+h*eval(f,{'t':t[i],'y':y[i]}))

    return [t,y[:-1]]

## inciso a
t0 = 0
y0 = 1
T = 10

N = [10,50,100,500,1000]

error = []

fig1,ax1 = plt.subplots()
fig2,ax2 = plt.subplots()

for i in N:
    
    t,y = resuelve_y(y0,t0,T,i)

    yreal = (3/4)*np.exp(-2*t)+(t**2-t+1/2)/2

    ax1.plot(t,y,label = 'N = ' + str(i))
    error.append(np.abs(y[-1]-yreal[-1]))

ax1.plot(t,yreal,label = 'solución analítica')
ax2.plot(1/np.array(N),error,'o')
ax1.legend()
plt.show()

## inciso b

h=0.1
t = np.arange(0,10+h,h)

x0 = 1 #Llamo x a y'
y0 = 0

xA = [x0]
yA = [y0]

xB = [x0]
yB = [y0]

xC = [x0]
yC = [y0]
for i in range(len(t)):

    yA.append(yA[i]+h*xA[i])
    xA.append(xA[i]-2*h*yA[i])
    
    xB.append(xB[i]-2*h*yB[i])
    yB.append(yB[i]+h*xB[i+1])

    yC.append(yC[i]+h*xC[i])
    xC.append(xC[i]-2*h*yC[i+1])

yreal = np.sin(np.sqrt(2)*t)/np.sqrt(2)
yA = np.array(yA[:len(t)])
yB = np.array(yB[:len(t)])
yC = np.array(yC[:len(t)])
fig,ax = plt.subplots()

ax.plot(t,yA,label = 'opc A')
ax.plot(t,yB,label = 'opc B')
ax.plot(t,yC,label = 'opc C')
ax.plot(t,yreal,label = 'solucion analitica') 
ax.legend()
ax.grid()

fig2,ax2 = plt.subplots()
ax2.set_title('errores')
ax2.plot(t,np.abs(t,yA-yreal),label = 'opc A')
ax2.plot(t,np.abs(yB-yreal),label = 'opc B')
ax2.plot(t,np.abs(yC-yreal),label = 'opc C')
ax2.legend()

plt.show()

#Hasta acá la opción A es muy mala y las opciones B y C son indistingubles
print('el máximo error cometido por cada opción son B: ' + str(max(np.abs(yB-yreal))) + ', C: ' + str(max(np.abs(yC-yreal))))

#Las opciones B y C son indistinguibles, quizás se pueda hacer algo para determinar cual es la mejor opcion pero me quedo con la B solo por elegir alguna y seguir con el ejercicio

##inciso b errores en función de N

t0 = 0
y0 = 0
x0 = 1
T = 10

N = [10,50,100,500,1000]

t = [0]*len(N)
y = [0]*len(N)
error = []
errorpp = []

P = 2*np.pi/np.sqrt(2)
h = (T-t0)/(np.array(N)-1)
PpP = (P/h).astype(int) #Pasos por periodo

fig1,ax1 = plt.subplots()
fig2,ax2 = plt.subplots()
fig3,ax3 = plt.subplots()
for i in range(len(N)):
    

    t[i],y[i] = res_opcB(y0,x0,t0,T,N[i])
     
    yreal = np.sin(np.sqrt(2)*t[i])/np.sqrt(2)

    error.append(np.abs(y[i][-1]-yreal[-1]))
    ax2.plot(t[i],y[i],label = 'N = ' + str(N[i]))
    ax3.plot(np.arange(1,(T-t0)/P,1),np.array(y[i])[(PpP[i]*np.arange(1,(T-t0)/P,1)).astype(int)],label = 'N = ' + str(N[i]))
    
ax2.plot(t[-1],yreal,label = 'solucion analítica')
ax2.grid()
ax2.legend()
ax2.set_xlabel('Tiempo')
ax2.set_ylabel('y')
ax1.plot(1/np.array(N),error,'o')
ax1.set_xlabel('1/N')
ax1.set_ylabel('error')
ax3.set_xlabel('Nro de período')
ax3.set_ylabel('error')

plt.show()


#La solución respeta el periodo a partir de N=50

## inciso c
t,y = euler('y+t',0,0,10,100)
plt.plot(t,y)

