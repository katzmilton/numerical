##EJERCICIO 8
## Milton Ezequiel Katz	
## LU 683/16

import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as npl
import imageio as img

def a_grises(image,r,g):
    R = image[:,:,0] # Matriz de rojos
    G = image[:,:,1] # Matriz de grises
    B = image[:,:,2] # Matriz de azules
    gris = r*R + g*G + (1-r-g)*B
    return(gris)

def reduce_svd(A,p):
    U,s,V = npl.svd(A)
    n_elementos = int(p*len(s))
    s[-n_elementos:] = 0
    S = np.zeros((A.shape[0], A.shape[1]))
    S[:len(s), :len(s)] = np.diag(s)
    A_ = np.dot(U,np.dot(S,V))
    return(A_)

## Paso a escala de grises
image = img.imread('arbol.jpg',format='jpg')

r = 0.3
g = 0.59
imagen_gris = a_grises(image,r,g)
plt.imshow(imagen_gris,cmap='gray',vmin=0,vmax=255)
plt.savefig(f'ejemplo_en_escala_de_grises_r={r}_g={g}.jpg')

## Pruebo la reconstrucción del svd
U,s,V = npl.svd(imagen_gris)

S = np.zeros(imagen_gris.shape)
S[:len(s), :len(s)] = np.diag(s)
imagen_recuperada = np.dot(U,np.dot(S,V))

plt.imshow(imagen_recuperada,cmap='gray',vmin=0,vmax=255)
plt.savefig(f'ejemplo_en_escala_de_grises_r={r}_g={g}_recuperada.jpg')

## Pruebo reduce_svd
p = 0.99
imagen_reducida = reduce_svd(imagen_gris,p)

plt.imshow(imagen_reducida,cmap='gray',vmin=0,vmax=255)

##Error promedio

error = []
ps = np.linspace(0.8,0.99,10)
for p in ps: 
    print('Calculando con p='+str(p))
    image_ = reduce_svd(imagen_gris,p)
    error.append(np.mean(np.abs(image_-imagen_gris))/np.mean(imagen_gris))
    plt.imshow(image_,cmap='gray',vmin=0,vmax=255)
    plt.show()

fig,ax = plt.subplots()
ax.plot(ps,error,'o')
plt.show()

## Comparando imagenes
def curva_error(path_imagen,valores_p):
        
    image = img.imread(path_imagen,format='jpg')

    r = 1/3
    g = 1/3
    imagen_gris = a_grises(image,r,g)

    error = []
    for p in valores_p: 
        print('Calculando con p='+str(p))
        image_ = reduce_svd(imagen_gris,p)
        error.append(np.mean(np.abs(image_-imagen_gris))/np.mean(imagen_gris))
    return(error)

imagenes = ['arbol.jpg','cuadrado.jpg','fractal.jpg','mona_lisa.jpg','poligono.jpeg']
fig,ax = plt.subplots(2,3,sharex = True,sharey =True)
ps = np.linspace(0.5,0.99,10)

for i in range(len(imagenes)):
    print(f'Calculando error para {imagenes[i]}')
    error = curva_error(imagenes[i],ps)
    if i < 3:
        ax[0,i].plot(ps,error,'o')
        ax[0,i].set_title(imagenes[i])
    else:
        ax[1,i-3].plot(ps,error,'o')
        ax[1,i-3].set_title(imagenes[i])
plt.show()
