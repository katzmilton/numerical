## EJERCICIO 4
## Milton Ezequiel Katz	
## LU 683/16
##
import numpy as np
import matplotlib.pyplot as plt

def V_prima(coefs,V):
    S_prima = -coefs[0]*V[0]*V[1]/coefs[2]
    I_prima = coefs[0]*V[0]*V[1]/coefs[2]-coefs[1]*V[1]
    R_prima = coefs[1]*V[1]
    return(np.array([S_prima,I_prima,R_prima]))

def paso_runge_kutta(coefs,V,dt,t):
    k1 = V_prima(coefs,V)
    k2 = V_prima(coefs,V+dt*k1/2)
    k3 = V_prima(coefs,V+dt*k2/2)
    k4 = V_prima(coefs,V+dt*k3)
    V += dt*(k1+2*k2+2*k3+k4)/6
    t += dt
    return([t,V])

def integra_runge_kutta(coefs,V0,t0,T,dt):
    t = [t0]
    S = [V0[0]]
    I = [V0[1]]
    R = [V0[2]]
    while t[-1]<T:
        t_sig,V_sig = paso_runge_kutta(coefs,np.array([S[-1],I[-1],R[-1]]),dt,t[-1])
        t.append(t_sig)
        S.append(V_sig[0])
        I.append(V_sig[1])
        R.append(V_sig[2])
    
    return([np.array(t),np.array(S),np.array(I),np.array(R)])

##
coefs = [1.3,1,1e4]
V0 = coefs[2]*np.array([0.99,0.01,0])
t0 = 0
T = 1000
dt = 0.15

t,S,I,R = integra_runge_kutta(coefs,V0,t0,T,dt)
fig,ax = plt.subplots()
ax.plot(t,S,label = 'Susceptibles')
ax.plot(t,I,label = 'Infectados')
ax.plot(t,R,label = 'Recuperado')
ax.plot(t,S+I+R,label = 'Población total')
ax.grid()
fig.legend(loc = (0.65,0.65))
plt.show()

##
coefs = [5,1,1e4]
V0 = coefs[2]*np.array([0.99,0.01,0])
t0 = 0
T = 10
dt = 0.15

t,S,I,R = integra_runge_kutta(coefs,V0,t0,T,dt)
fig,ax = plt.subplots()
ax.plot(t,S,label = 'Susceptibles')
ax.plot(t,I,label = 'Infectados')
ax.plot(t,R,label = 'Recuperado')
ax.grid()
fig.legend(loc = (0.65,0.65))
plt.show()
#a medida que beta es más chico la curva se aplana
##
beta = np.arange(0.5,10,0.1)
t0 = 0
T = 10
dt = 0.15
maxI = []
for beta_i in beta:
    coefs = [beta_i,1,1e4]
    V0 = coefs[2]*np.array([0.99,0.01,0])
    maxI.append(max(integra_runge_kutta(coefs,V0,t0,T,dt)[2]))

fig,ax = plt.subplots()
ax.plot(beta,maxI)
ax.grid()
ax.set_xlabel('$\\beta$')
ax.set_ylabel('Infectados')
plt.show()


