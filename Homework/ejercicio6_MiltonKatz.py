## EJERCICIO 6
## Milton Ezequiel Katz	
## LU 683/16
import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt

def jacobi(A,b,x0,max_iter,tol):
    D = np.diag(np.diag(A))
    L = np.tril(A)-D
    U = np.triu(A)-D
    M = npl.inv(D)
    N = U + L
    B = -np.dot(M,N)
    C = np.dot(M,b)
    xN = x0
    resto = npl.norm(A*xN-b)
    iter = 0

    while iter < max_iter and tol < resto:
        xN = np.dot(B,xN)+C
        iter = iter + 1
        resto = npl.norm(A*xN-b)
        if iter == max_iter:
            print('Max_iter reached')

    return([xN,iter])


def gauss_siegel(A,b,x0,max_iter,tol):
    D = np.diag(np.diag(A))
    L = np.tril(A)-D
    U = np.triu(A)-D
    M = npl.inv(D + L)
    N = U
    B = -np.dot(M,N)
    C = np.dot(M,b)
    xN = x0
    resto = npl.norm(A*xN-b)
    iter = 0

    while iter < max_iter and tol < resto:
        xN = np.dot(B,xN)+C
        iter = iter + 1
        resto = npl.norm(A*xN-b)
        if iter == max_iter:
            print('Max_iter reached')

    return([xN,iter])

def matriz_y_vector(n):
    A = np.random.rand(n, n)
    while npl.det(A) == 0:
        A = np.random.rand(n, n)
    b = np.random.rand(n,1)
    return([A,b])
##

trials_J = []
trials_GS = []

Ntrials = 10
trials = 0
x0 = np.transpose(np.array([[0,0,0]]))
max_iter = 10**6
tol = 10**-5
while trials<Ntrials:
    A,b = matriz_y_vector(3)
    rGS= gauss_siegel(A,b,x0,max_iter,tol)
    rJ = jacobi(A,b,x0,max_iter,tol)
    trials_J.append(rGS[1])
    trials_GS.append(rJ[1])
    trials = trials + 1

plt.scatter(trials_J,trials_GS)
plt.ylabel('Gauss-Siegel')
plt.xlabel('Jacobi')
plt.yscale('log')
plt.xscale('log')
plt.show()
