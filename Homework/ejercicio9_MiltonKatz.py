## EJERCICIO 9
## Milton Ezequiel Katz	
## LU 683/16

import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt
import pandas as pd

def matriz_A(x,n,tipo):
    nx = len(x)
    A = np.zeros((nx,n))
    if tipo=='polinomial':
        for i in range(nx):
            for j in range(n):
                A[i][j] = x[i]**j
    elif tipo=='senoidal':
        for i in range(nx):
            for j in range(n):
                A[i][j] = np.sin((j+1)*x[i])
    return(A)

def cuadrados(x,y,n,tipo):
    A = matriz_A(x,n,tipo)
    B = np.dot(A.T,A)
    c = np.dot(npl.inv(B) ,np.dot( A.T , y))
    return(c)

def genera_ajustador(c,tipo):
    if tipo=='polinomial':
        def function(z):
            w = 0
            for j in range(len(c)):
                w += c[j] * z**j
            return(w)
    elif tipo=='senoidal':
        def function(z):
            w = 0
            for j in range(len(c)):
                w += c[j] * np.sin((j+1)*z)
            return(w)
    return(function)
## Pruebo la funcion matriz_A
A1 = matriz_A(np.array([0,2]),3,'senoidal')
A2 = matriz_A(np.array([0,2]),3,'polinomial')

## Chequeo la funcion cuadrados
x = np.array([0,1,2])
y = np.array([0,1,4])
c = cuadrados(x,y,3,'polinomial')
print(c)

## inciso A y D

X = np.linspace(0,np.pi,10)
Y = np.sin(X)

c_pol = cuadrados(X,Y,3,'polinomial')
c_sin = cuadrados(X,Y,3,'senoidal')

print(c_pol)
print(c_sin)

## Pruebo la funcion genera_ajuste

x = np.linspace(min(X),max(X),50)
ajusta_pol = genera_ajustador(c_pol,'polinomial')
ajusta_sin = genera_ajustador(c_sin,'senoidal')

fig,ax = plt.subplots(2)
ax[0].plot(x,ajusta_pol(x),label = 'Ajuste polinómico')
ax[0].plot(X,Y,'o',label = 'datos')
ax[1].plot(x,ajusta_sin(x),label = 'Ajuste senoidal')
ax[1].plot(X,Y,'o',label = 'Datos')
ax[0].legend()
ax[1].legend()
plt.show()

## Inciso F

data = pd.read_csv('ejercicio7/data_prob7.csv')
data.area_acres = np.log10(data.area_acres)
valores_n = [2,4,6,8]

fig,ax = plt.subplots()
ax.plot(*data.values.T,'o',label = 'Datos')
x = np.linspace(min(data.area_acres),max(data.area_acres),50)


for n in valores_n:
    c = cuadrados(*data.values.T,n,'polinomial')
    ajusta_pol = genera_ajustador(c,'polinomial')
    ax.plot(x,ajusta_pol(x),label = f'polinomio grado {n}')

ax.legend()
plt.show()

