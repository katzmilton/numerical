##EJERCICIO 5
##MILTON KATZ
##LU 683/16
import matplotlib.pyplot as plt
import numpy as np
import imageio

def matriz_evolucion(N,k,h):
    lam = k/h
    resultado = (1-2*lam**2)*np.identity(N) 
    resultado[np.arange(N-1),np.arange(N-1)+1] = (N-1)*[lam] 
    resultado[np.arange(1,N),np.arange(1,N)-1] = (N-1)*[lam] 
    return(resultado) 


def f_x(x):
    f = np.array([0]*len(x)) 
    
    return(f) # f es la condicion inicia u(x,t=0)

def g_x(x):
    g = np.array(len(x)*[0])
    g[int(len(x)/3):int(len(x)*2/3)] = 1e3
    return(g) # g es la condicion inicial du/dt(x,t=0)

def cumple_contorno(solucion,contornos):
    solucion[0] = contornos[0]
    solucion[-1] = contornos[1]
    return(solucion) # Retorno solucion con los valores cambiados en los contornos

def gen_condicion_inicial(x,k):
    condicion_0 = list(f_x(x))
    condicion_1 = list(f_x(x) + k*g_x(x))
    return([condicion_0,condicion_1]) # Retorna una lista con las dos condiciones iniciales
    

def paso_integracion(u,u_,contornos,A): # Esta vez, ya vamos a tomar como construida la matriz
    uN = np.dot(A,u)-u_
    uN = cumple_contorno(uN,contornos)
    return(uN) #empleando u y u_, además de la matriz, verifico que se cumpla la condicion de contorno y calculo uN

def integra(x,contornos,k,t0,T):
    h = x[1]-x[0]
    condicion_inicial = gen_condicion_inicial(x,k)
    t = np.arange(t0,T,k)
    A = matriz_evolucion(len(x),k,h)
    solucion = condicion_inicial
    for i in range(1,len(t)-1):
        solucion.append(paso_integracion(solucion[i],solucion[i-1],contornos,A))

    return([t,solucion]) # Realiza una y otra vez los pasos de integracion
    

## Ahora evaluamos condiciones iniciales y valores de contorno    
contornos = [0,0] # 0 en ambos extremos
N = 100 # Cantidad de pasos en la linea
L = np.pi
h = L/N # Paso espacial, notar el N-1. Prueben sacar el -1 a ver que pasa
k = h**2/10 # Paso temporal
T = 8 # Tiempo final
t0 = 0 # tiempo inicial
x = np.arange(0,L,h)

#INTEGRAMOS
condicion_inicial = gen_condicion_inicial(x,k) 
t,u = integra(x,contornos,k,t0,T)

# GRAFICAMOS
plt.plot(x,u[1000]) # Miremos una solucion
# plt.savefig('condicion_inicial.png')
# Primero armemos una función que arme una imagen
##
def grafica_t(soluciones, indice,k,h):  
    plt.clf()     # borramos lo que hubiera antes en la figura
    x = np.arange(len(soluciones[indice]))*h # Genero una tira de valores x
    u = soluciones[indice] # agarro la solución indicada
    t = indice*k # Calculo el tiempo
    # grafico la curva en ese tiempo
    plt.plot (x,u,label='t='+str(np.round(t,4)))
    plt.ylim(-1.5,1.5) # Para que se vea toda la soguita
    plt.legend()
    
# Y ahora armamos una función que nos haga un videito o un gif
def video_evolucion(soluciones, nombre_video,k,h):
    lista_fotos =[] # aca voy a ir guardando las fotos
    for i in range ( len(soluciones)):
        if i %500==0: # esto es para guardar 1 de cada 500 fotos y sea menos pesado el giff. Si se les corta el guardado de imagen en repl, prueben aumentar este numero
            grafica_t(soluciones, i, k, h)
            plt.savefig(nombre_video + '.png')
            lista_fotos.append(imageio.imread(nombre_video + '.png'))
            print (str(i) + ' de '+ str(len(soluciones)) + ' fotos guardadas')
    imageio.mimsave(nombre_video + '.gif', lista_fotos) # funcion que crea el video
    print('Gif Guardado')
    
video_evolucion(u,'gif_ondas',k,h)


