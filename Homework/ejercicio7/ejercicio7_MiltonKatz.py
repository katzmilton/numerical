## EJERCICIO 7
## Milton Ezequiel Katz	
## LU 683/16
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as npl
import pandas as pd

def matriz_dif(x):
    A = np.zeros((len(x),len(x)))
    A[:,0] = 1
    for q in range(len(x)):
        for r in range(1,q+1):
            if q >= r:
                A[q,r] = 1
                i = 0
                while i < r:
                    A[q,r] *= (x[q] - x[i])
                    i += 1
    return(A)

def jacobi(A,b,x0,max_iter,tol):
    D = np.diag(np.diag(A))
    L = np.tril(A)-D
    U = np.triu(A)-D
    M = npl.inv(D)
    N = U + L
    B = -np.dot(M,N)
    C = np.dot(M,b)
    xN = x0
    resto = npl.norm(A*xN-b)
    iter = 0

    while iter < max_iter and tol < resto:
        xN = np.dot(B,xN)+C
        iter = iter + 1
        resto = npl.norm(A*xN-b)
        if iter == max_iter:
            print('Max_iter reached')

    return([xN,iter])

def newton_dif(x,y,max_iter,tol):
    A = matriz_dif(x)
    b = y 
    x0 = np.zeros(len(y))
    coef,_ = jacobi(A,b,x0,max_iter,tol)
    return(coef)

def funcion_interpol(x,y,max_iter,tol):
    coef = newton_dif(x,y,max_iter,tol)
    def function(m):
        X = np.ones(len(coef))
        for i in range(1,len(coef)):
            X[i] = np.prod(m - x[:i])
        return(np.dot(coef,X))
    return(function)

tol = 1e-3
max_iter = 1e3

x = np.linspace(0,np.pi,10)
y = np.sin(x)

interpol_sin = funcion_interpol(x,y,max_iter,tol)
x_interpol = np.linspace(0,np.pi,100)
y_interpol = []
for i in x_interpol:
    y_interpol.append(interpol_sin(i))

fig,ax = plt.subplots()
ax.plot(x_interpol,y_interpol,label='Polinomio interpolador')
ax.plot(x_interpol,np.sin(x_interpol),label = 'sen(x)')
ax.plot(x, y,'o', label='datos')
ax.legend()

datos = pd.read_csv('data_prob7.csv')
x = np.log2(np.array(datos['area_acres']))
y = np.array(datos['pop_log2'])
interpol_datos = funcion_interpol(x,y,max_iter,tol)
x_interpol = np.linspace(min(x),max(x),100)
y_interpol = []
for i in x_interpol:
    y_interpol.append(interpol_datos(i))

fig_dat,ax_dat = plt.subplots()
ax_dat.plot(x_interpol,y_interpol,label='Polinomio interpolador')
ax_dat.plot(x, y,'o', label='datos')
ax_dat.legend()
plt.show()

