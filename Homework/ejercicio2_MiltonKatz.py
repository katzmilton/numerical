## EJERCICIO 2
## Milton Katz
## LU 683/16.

##
import numpy as np
import matplotlib.pyplot as plt
##

def primos_hasta(N):

    primos = np.arange(2,N+1)
    divs = np.arange(2,int(N/2))
    for i in divs:
        primos = primos[(primos==i)|(primos%i!=0)]
    
    return primos

def cuantos_primos(N,L):
    
    primos = primos_hasta(N)
    primEnL = primos[np.isin(primos,L)]

    return len(primEnL)
    
##
N = 10**3
L = primos_hasta(N)
x = np.arange(1,N+1)
y = []

for i in x:
    y.append(cuantos_primos(i,L))

fig, ax = plt.subplots()
a = 0.12
b = 100
ax.plot(x,a*x+b)
ax.plot(x,np.log(x))
ax.plot(x,np.sqrt(x))
ax.plot(x,y)
ax.grid()
ax.set_xlabel('Hasta')
ax.set_ylabel('Catidad de primos')
plt.show()

plt.show()

    
