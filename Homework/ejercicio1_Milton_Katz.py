## EJERCICIO 1
## Milton Ezequiel Katz	
## LU 683/16

# La ecuación de la recta es y=m*x+b
y1 = 10
y2 = 100
x1 = 10
x2 = 25

m = (y2-y1)/(x2-x1)
b = y1 - m*x1
print('El valor de m es',m)
print('El valor de b es',b)


## La ecuación de la recta es y=b*e**(m*x)
y1 = 10
y2 = 100
x1 = 10
x2 = 25

import numpy as np

m = np.log(y2/y1)/(x2-x1)
b = y1/np.exp(m*x1)	

print('El valor de m es',m)
print('El valor de b es',b)
