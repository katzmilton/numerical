import numpy as np
import matplotlib.pyplot as plt

h=0.01
t = np.arange(0,10,h)
x0 = 1
x = []


for i in range(len(t)):
	if i == 0:
		x.append(x0)
	else: 
		T = (t[i-1]+2)*x[i-1]/(t[i-1]+1) + (h/2)*(((t[i-1]+1)-(t[i-1]+2))*x[i-1]/(t[i-1]+2)**2 + ((t[i-1]+1)/(t[i-1]+2))**2 *x[i-1])
		x.append(x[i-1] + h*T)

fig, ax = plt.subplots()
ax.plot(t,x)
ax.plot(t,(t+1)*np.exp(t))
plt.show()
