import numpy as np
import matplotlib.pyplot as plt

h=0.01
t = np.arange(0,1+h,h)

x0 = 1

x = [x0]

for i in range(len(t)):
	
	x.append(x[i]+h*x[i])


fig,ax = plt.subplots()
ax.plot(t,x[:len(t)])
ax.plot(t,np.exp(t))

print('El valor de e es: ', x[int(1/h)])
print('El que da numoy es: ', np.e)
dif = x[int(1/h)]-np.e
print('La diferencia: ', dif)

plt.show()

